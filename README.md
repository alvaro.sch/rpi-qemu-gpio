# rpi-qemu-gpio

A simple way to emulate GPIO interaction in qemu for BCM283X (tested raspberry-pi 3b), written for a computer lab assignment of 'Computer Organization' class at FAMAF UNC.

## Compiling

It should work out-of-the box in most unix machines with a c compiler.

i.e.

```sh
$ gcc gpiom.c
```

## Connecting with qemu

Add the following arguments when starting qemu `-qtest unix:/tmp/qtest.sock,server,nowait`.

i.e.

```sh
$ qemu-system-aarch64 ... -qtest unix:/tmp/qtest.sock,server,nowait
```

Then run the executable.

## Holding a key

key-helds are propperly implemented, however due to [typematic delay](https://wiki.archlinux.org/title/Xorg/Keyboard_configuration#Adjusting_typematic_delay_and_rate) it will register as a release and hold, replacing *termios* with i.e. *ncurses* or other more advanced terminal API may fix the issue.

